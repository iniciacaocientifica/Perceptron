# coding: utf-8
import math

from Pioneer.vrepConst import *

from Pioneer.vrep import *

class RobotWorld(object):
    '''
    Robot simulator class to communicate with the simulation environment
    :return: 0 on success, -1 otherwise
    '''

    def __init__(self, host, port_number):
        self._host = host
        self._port = port_number
        print('Program started')
        simxFinish(-1)  # just in case, close all opened connections
        self.clientID = simxStart(self._host, self._port, True, True, 5000, 5)  # Connect to V-REP
        if self.clientID == -1:
            print("Failed to connect to Remote API Server")
        else:
            print('Connected to Remote API Server')

    def getClientID(self):
            return self.clientID

    def act(self, robot, command, dParams):
        '''
        Implements action commands in the robot world
        :param robot: the robot we want to control
        :param command: action command to execute
        :param dParams: action parameters
        :return: True if action was actuated
        '''
        assert isinstance(robot, RobotBrain)

        if command == 'avoid':
            velocityLeft = 1                                                  # Using Braitenberg's Algo, it will determinate the right correction
            velocityRight = 1                                                 # Velocities driven by Sensors
            detected = robot.getState()
            brait_left = (-0.2,-0.4,-0.6,-0.8,-1,-1.2,-1.4,-1.6)              # Braitenberg Weights took from Lua script
            brait_right = (-1.6,-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2)
            for i in range(0,8):
                velocityLeft += brait_left[i] * detected[i]                               # Calculating new Velocities
                velocityRight += brait_right[i] * detected[i]
            print('VL: ', velocityLeft)
            print('VR: ', velocityRight)
            for s in robot.jointDesc:                                                                   # Retrive Joints from the robot "memory"
                if robot.jointDesc[s][0] == sim_object_joint_type & ('left' in s):
                    leftJoint = robot.jointDesc[s][1]
                if robot.jointDesc[s][0] == sim_object_joint_type & ('right' in s):
                    rightJoint = robot.jointDesc[s][1]
            simxSetJointTargetVelocity(self.clientID,leftJoint, velocityLeft, simx_opmode_blocking)     # Setting Velocities
            simxSetJointTargetVelocity(self.clientID,rightJoint, velocityRight, simx_opmode_blocking)   # Setting Velocities
        return True

    def _readSensor(self, sensorHandle, sensorType):
        if sensorType == sim_object_forcesensor_type:
            result, s, f, t = simxReadForceSensor(self.clientID, sensorHandle, simx_opmode_blocking)
            return s, f, t
        elif sensorType == sim_object_proximitysensor_type:
            noDetectionDist = 0.5
            maxDetectionDist = 0.2
            emptyBuff = bytearray()
            res, retInts, dist, retStrings, retBuffer = simxCallScriptFunction(self.clientID, 'readDistance', sim_scripttype_childscript,'readDistance_function', [sensorHandle], [], [], emptyBuff, simx_opmode_blocking)
            if len(dist) > 0:
                if dist[0] < noDetectionDist:
                    if dist[0] < maxDetectionDist:
                        return maxDetectionDist
                    else:
                        return dist[0]
                else:
                    return noDetectionDist
            else:
                return noDetectionDist
        elif sensorType == sim_object_visionsensor_type:
            result, s, auxPackets = simxReadVisionSensor(self.clientID, sensorHandle, simx_opmode_blocking)
            return s, auxPackets
        else:
            print('ERROR')
            # TODO: Raise exception

    def sense(self, robot):
        '''
        Implements sensor reading from the robot simulator
        :param robot: robot of which we ask the sense
        :return: sensor readings
        '''
        assert isinstance(robot, RobotBrain)
        out = {}
        for sensor in robot.sensorDesc:
            out[sensor] = self._readSensor(robot.sensorDesc[sensor][1], robot.sensorDesc[sensor][0])
        return out

    def close(self):
        '''
        Close connection with the simulator
        :return: void
        '''
        # Before closing the connection to V-REP, make sure that the last command sent out had time to arrive.
        # You can guarantee this with (for example):
        simxGetPingTime(self.clientID)
        # Now close the connection to V-REP:
        simxFinish(self.clientID)


class RobotBrain(object):
    '''
    The main intelligent controller of the simulated robot
    '''

    def __init__(self, world, robotName):
        '''
        :param world: The world in which the robot is
        :param robotName: Name of the robot
        '''
        assert isinstance(world, RobotWorld)
        self.sensorDesc = {}
        self.jointDesc = {}
        self._state = [0, 0, 0, 0, 0, 0, 0, 0]
        self.world = world
        # Retrives all Sensors and all Joint from the robot, building the robot Memory
        # Starting from ProximitySensors
        for i in range(0,8):
            s = 'Pioneer_p3dx_ultrasonicSensor'+ str(i+1)
            res, sensor = simxGetObjectHandle(world.getClientID(), s, simx_opmode_blocking)
            self.sensorDesc[s] = [sim_object_proximitysensor_type, sensor]

        # Retriving Joints
        res, joint = simxGetObjectHandle(world.getClientID(), "Pioneer_p3dx_leftMotor", simx_opmode_blocking)
        self.jointDesc["Pioneer_p3dx_leftMotor"] = [sim_object_joint_type, joint]
        res, joint = simxGetObjectHandle(world.getClientID(), "Pioneer_p3dx_rightMotor", simx_opmode_blocking)
        self.jointDesc["Pioneer_p3dx_rightMotor"] = [sim_object_joint_type, joint]

    def getState(self):
        return self._state

    def perception(self, externalInput):
        '''
        Read state and build a world representation in his 'state'
        :param externalInput: what is the sensor layer
        :return: True if he percepts successfully
        '''

        for i in range(0,8):
            s = 'Pioneer_p3dx_ultrasonicSensor' + str(i + 1)
            self._state[i] = 1 - (externalInput[s] - 0.2) / (0.5 - 0.2)
        return True

    def decision(self):
        '''
        The state contains the world's representation
        :return: the took decision
        '''
        return 'avoid'

    def think(self, sensorReadings):
        '''
        It decides which action to take given sensor readings
        :param sensorReadings: collection of sensor readings
        :return: action
        '''
        assert isinstance(sensorReadings, dict)
        self.perception(sensorReadings)
        action = self.decision()
        return action