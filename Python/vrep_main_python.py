# -*- coding: utf-8 -*-
"""
Created on Sun Apr 02 15:05:27 2017

@author: sergi
"""

import vrep
import sys

import numpy as np
import matplotlib.pyplot as mlp


### CONECTION INITIALIZATION ###
vrep.simxFinish(-1) # just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP

if clientID!=-1:            #return -1 when connection is not stablished 
    print ('Connected to remote API server')                       
else:
    print ('Connection not successful')                       
    sys.exit('Could not Connect')


### MOTORS ###
errorCode, left_motor_handle = vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_leftMotor', vrep.simx_opmode_blocking)
errorCode, right_motor_handle = vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_rightMotor', vrep.simx_opmode_blocking)

errorCode = vrep.simxSetJointTargetVelocity(clientID, left_motor_handle, 0.2, vrep.simx_opmode_streaming)
errorCode = vrep.simxSetJointTargetVelocity(clientID, right_motor_handle, 0.6, vrep.simx_opmode_streaming)


### PROXIMITY SENSORS ###
#get the handle function of sensor
errorCode, sensor1 = vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor1', vrep.simx_opmode_blocking)
#inicialize reading of proximity sensor by using simx_opmode_streaming
returnCode,detectionState,detectedPoint, detectedObjectHandle, detectedSurfaceNormalVector=vrep.simxReadProximitySensor(clientID,sensor1,vrep.simx_opmode_streaming)
#get values of sensor by using simx_opmode_buffer
returnCode,detectionState,detectedPoint, detectedObjectHandle, detectedSurfaceNormalVector=vrep.simxReadProximitySensor(clientID,sensor1,vrep.simx_opmode_buffer)

### CAMERA ###
#get the handle function of cameras
errorCode, cam_handle = vrep.simxGetObjectHandle(clientID,'cam1', vrep.simx_opmode_blocking)
#inicialize reading of camera by using simx_opmode_streaming
returnCode,resolution,image=vrep.simxGetVisionSensorImage(clientID, cam_handle,0,vrep.simx_opmode_streaming)
#get values of sensor by using simx_opmode_buffer
returnCode,resolution,image=vrep.simxGetVisionSensorImage(clientID, cam_handle,0,vrep.simx_opmode_buffer)

### TRATA DADOS DA CAMERA ###
#im=np.array(image, dtype=np.uint8) #converte valores de vetor image para remover parte negativa
#im.resize([resolution[0], resolution[1], 3]) #cooca no padrao RGB 
#mlp.imshow(im, origin = 'lower') #exibe imagem rbg