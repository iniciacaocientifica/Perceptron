import numpy as np
import matplotlib.pyplot as mpl   #used for image plotting


sample = np.array([[ 0.98480775, -0.17364818,  0.        ],
                   [ 1.96961551, -0.34729636,  0.        ],
                   [ 2.95442326, -0.52094453,  0.        ],
                   [ 3.93923101, -0.69459271,  0.        ],
                   [ 4.92403877, -0.86824089,  0.        ],
                   [ 5.90884652, -1.04188907,  0.        ],
                   [ 6.89365427, -1.21553724,  0.        ],
                   [ 7.87846202, -1.38918542,  0.        ],
                   [ 8.86326978, -1.5628336 ,  0.        ],
                   [ 9.84807753, -1.73648178,  1.        ],
                   [ 0.98480775,  0.17364818,  0.        ],
                   [ 1.96961551,  0.34729636,  0.        ],
                   [ 2.95442326,  0.52094453,  0.        ],
                   [ 3.93923101,  0.69459271,  0.        ],
                   [ 4.92403877,  0.86824089,  1.        ],
                   [ 5.90884652,  1.04188907,  1.        ],
                   [ 6.89365427,  1.21553724,  1.        ],
                   [ 7.87846202,  1.38918542,  1.        ],
                   [ 8.86326978,  1.5628336 ,  1.        ],
                   [ 9.84807753,  1.73648178,  1.        ],
                   [ 0.8660254 ,  0.5       ,  0.        ],
                   [ 1.73205081,  1.        ,  0.        ],
                   [ 2.59807621,  1.5       ,  0.        ],
                   [ 3.46410162,  2.        ,  0.        ],
                   [ 4.33012702,  2.5       ,  1.        ],
                   [ 5.19615242,  3.        ,  1.        ],
                   [ 6.06217783,  3.5       ,  1.        ],
                   [ 6.92820323,  4.        ,  1.        ],
                   [ 7.79422863,  4.5       ,  1.        ],
                   [ 8.66025404,  5.        ,  1.        ],
                   [ 0.64278761,  0.76604444,  0.        ],
                   [ 1.28557522,  1.53208889,  0.        ],
                   [ 1.92836283,  2.29813333,  0.        ],
                   [ 2.57115044,  3.06417777,  0.        ],
                   [ 3.21393805,  3.83022222,  0.        ],
                   [ 3.85672566,  4.59626666,  0.        ],
                   [ 4.49951327,  5.3623111 ,  1.        ],
                   [ 5.14230088,  6.12835554,  1.        ],
                   [ 5.78508849,  6.89439999,  1.        ],
                   [ 6.4278761 ,  7.66044443,  1.        ]])

  
def perceptron(data):
    
    bias = 0
    counter = [1]
    weight = [0,0]
    n=0.1
    
    while np.dot(counter,counter):

        counter = []
        a = 0
        for item in data:
    
            target = item[2]
            learning = weight[0]*item[0] + weight[1]*item[1] + bias
    
            if learning >= 0:
                output = 1
            else:
                output = 0
                
            error = target - output
            bias += error
            weight[0] += n*error*item[0]
            weight[1] += n*error*item[1]
            counter.append(error)
#            print ("iteração:" + str(a))
            a = a + 1
            print bias
    mpl.plot([data[i,1] for i in range(0, len(data)) if data[i,2]==1 ], 
             [data[i,0] for i in range(0,len(data)) if data[i,2]==1 ],
             "ro")
    mpl.plot([data[i,1] for i in range(0,len(data)) if data[i,2]==0 ], 
             [data[i,0] for i in range(0,len(data)) if data[i,2]==0 ],
             "go")
    mpl.plot(weight[1], weight[0],"go")
    
    ax = mpl.axes()
    ax.arrow(0, 0, weight[1] , weight[0], head_width=0.05, head_length=0.1, fc='k', ec='k')  
    mpl.show() 
    
    
    return weight

view = perceptron(sample)



print(view)