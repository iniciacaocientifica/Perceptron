# -*- coding: utf-8 -*-
"""
Created on Sun Apr 02 15:05:27 2017

@author: sergi
"""

#Import Libraries:
import vrep                  #V-rep library
import sys
import time                #used to keep track of time
import numpy as np         #array library
import math
import matplotlib.pyplot as mpl   #used for image plotting

#import perceptron_test as pcpt


def perceptron(data,prn):
    
    weight = np.array([0,0,0])    # x, y e bias
    n = 0.1             #variável para controlar quanto o erro muda no valor de w
    
    flag_erro = True
    while flag_erro:

        for item in data:
            target = item[2]
            learning = weight[0]*item[0] + weight[1]*item[1] + (-1)
    
            if learning >= 0:
                output = 1
            else:
                output = 0
            
            error = output - target
            
            if error == 0:
                flag_erro = False
                                   
            weight = weight + n*error*np.array([item[0], item[1], -1])
            
    if prn == True:                    
        mpl.plot([data[i,0] for i in range(0, len(data)) if data[i,2]==1 ], 
                 [data[i,1] for i in range(0,len(data)) if data[i,2]==1 ],
                 "ro")
        mpl.plot([data[i,0] for i in range(0,len(data)) if data[i,2]==0 ], 
                 [data[i,1] for i in range(0,len(data)) if data[i,2]==0 ],
                 "go")
        mpl.plot(weight[0], weight[1],"bo")
        
        ax = mpl.axes()
        ax.arrow(0, 0, weight[0] , weight[1], head_width=0.05, head_length=0.1, fc='k', ec='k')
        mpl.show() 
        print "-"

    return weight

def detecta_pontos(imprime):
    '''Area de programa feito para detectar pontos dependnedo da distancia do sensor, entretanto
    não funcionou pois a função do VREP simplesmente não retorna os valores esperados'''
    r = np.zeros(shape = (8, 10))
    perceptron_points = np.zeros(shape = (80,3)) #80 pontos detectados, valores: x y detectado
    max_detect = 0.5 #metros, tamanho de 1 quadrado no vrep                 
    
    for i in range(0, 8):
        if detections[i] == 1:
            if sensor_val[i] > 0.9*max_detect:
                r[i]= np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
            elif sensor_val[i] > 0.8*max_detect:
                r[i]= np.array([0, 0, 0, 0, 0, 0, 0, 0, 1, 1])
            elif sensor_val[i] > 0.7*max_detect:
                r[i]= np.array([0, 0, 0, 0, 0, 0, 0, 1, 1, 1])
            elif sensor_val[i] > 0.6*max_detect:
                r[i]= np.array([0, 0, 0, 0, 0, 0, 1, 1, 1, 1])
            elif sensor_val[i] > 0.5*max_detect:
                r[i]= np.array([0, 0, 0, 0, 0, 1, 1, 1, 1, 1])
            elif sensor_val[i] > 0.4*max_detect:
                r[i]= np.array([0, 0, 0, 0, 1, 1, 1, 1, 1, 1])
            elif sensor_val[i] > 0.3*max_detect:
                r[i]= np.array([0, 0, 0, 1, 1, 1, 1, 1, 1, 1])
            elif sensor_val[i] > 0.2*max_detect:
                r[i]= np.array([0, 0, 1, 1, 1, 1, 1, 1, 1, 1])
            elif sensor_val[i] > 0.1*max_detect:
                r[i]= np.array([0, 1, 1, 1, 1, 1, 1, 1, 1, 1])
            else:
                r[i]= np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])     
        else:
           r[i]= np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) 


    '''SELECIONA DADOS PARA SER PROCESSADOS PELO PERCEPTRON'''
    '''DIVIDE DADOS DE SENSORES DA ESQUERDA E DIREITA'''

    weight = np.zeros((8,3))
    for k in range(0,8):
        data = np.array([[0,0,0]])        
        for i in range(1*k, 1*k+1):
            for j in range(0, 10):
                perceptron_points[10*i + j, 0] = (j + 1)* math.cos(sensor_loc[i])
                perceptron_points[10*i + j, 1] = (j + 1)* math.sin(sensor_loc[i])
                perceptron_points[10*i + j, 2] = r[i,j]   
             
                if detections[i] == 1:
                    data = np.concatenate((data, [perceptron_points[10*i + j]]))
        data = np.delete(data,0,0)
        
        if data.any():
            weight[k] = perceptron(data, imprime)
        else:
            weight[k] = np.array([0,0,0])

        
    return weight

def imprime_vetores():
    ax = mpl.axes()    
    #desenha eixos do robo
    
    ax.arrow(rob_pos[0], rob_pos[1], rob_pos_global_x[0], rob_pos_global_x[1], head_width=0.3, head_length=0.5, color = 'b')
    ax.arrow(rob_pos[0], rob_pos[1], rob_pos_global_y[0], rob_pos_global_y[1], head_width=0.3, head_length=0.5, color=(0,0.99,0))
    
    #desenha vetores para teste
    mpl.axis((-1,8,-1,8))
    mpl.plot(0,0,"x", color=(0,0,0))

    #desenha vetor objetivo local AZUL
    ax.arrow(rob_pos[0], rob_pos[1], vect_obj_local[0], vect_obj_local[1], head_width=0.3, head_length=0.5, color='b')
    
    #desenha vetor obstaculo local VERMELHO
    for z in range(0, vect_obs.size/3):
        ax.arrow(rob_pos[0], rob_pos[1], vect_obs[z,0], vect_obs[z,1], head_width=0.3, head_length=0.5, color=((0.99-z*0.1),0,0))

    #desenha vetor resultado local verde
    ax.arrow(rob_pos[0], rob_pos[1], vect_result[0], vect_result[1], head_width=0.3, head_length=0.5 , color='g')
    mpl.show()   
    print "-"
    
    
'''#--------------------------------------------------------
            INICIO DO CÓDIGO
   ---------------------------------------------------------'''
PI=math.pi  #pi=3.14..., constant
np.set_printoptions(threshold='nan')


### CONECTION INITIALIZATION ###
vrep.simxFinish(-1) # just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP

if clientID!=-1:            #return -1 when connection is not stablished 
    print ('Connected to remote API server')                       
else:
    print ('Connection not successful')                       
    sys.exit('Could not Connect')


### MOTORS HANDLES###
errorCode, left_motor_handle = vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_leftMotor', vrep.simx_opmode_blocking)
errorCode, right_motor_handle = vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_rightMotor', vrep.simx_opmode_blocking)

## ROBOT HANDLE
errorCode, robot_handle = vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx', vrep.simx_opmode_blocking)
## DUMMY HANDLE
# end point
errorCode, end_dummy_handle = vrep.simxGetObjectHandle(clientID,'end_point', vrep.simx_opmode_blocking)
#origin
errorCode, origin_handle = vrep.simxGetObjectHandle(clientID,'origin', vrep.simx_opmode_blocking)


errorCode, rob_pos = vrep.simxGetObjectPosition(clientID,robot_handle,origin_handle,vrep.simx_opmode_streaming)    #define robo como origem
errorCode, rob_ori = vrep.simxGetObjectOrientation (clientID,robot_handle,origin_handle,vrep.simx_opmode_streaming)    #define robo como origem
errorCode, end_pos = vrep.simxGetObjectPosition(clientID,end_dummy_handle,origin_handle,vrep.simx_opmode_streaming)   #define posição do final em relação ao robo 
errorCode, origin_pos = vrep.simxGetObjectPosition(clientID,origin_handle,-1,vrep.simx_opmode_streaming)   #define posição do final em relação ao robo 




sensor_h=[] #empty list for handles
sensor_val=np.array([]) #empty array for sensor measurements
detections = np.array([])


#orientation of all the sensors: 
sensor_loc=np.array([-PI/2, -50/180.0*PI,-30/180.0*PI,-10/180.0*PI,10/180.0*PI,30/180.0*PI,50/180.0*PI,PI/2,PI/2,130/180.0*PI,150/180.0*PI,170/180.0*PI,-170/180.0*PI,-150/180.0*PI,-130/180.0*PI,-PI/2]) 
sensor_loc=-sensor_loc
#for loop to retrieve sensor arrays and initiate sensors
for x in range(1,16+1):
    errorCode,sensor_handle=vrep.simxGetObjectHandle(clientID,'Pioneer_p3dx_ultrasonicSensor'+str(x),vrep.simx_opmode_oneshot_wait)
    sensor_h.append(sensor_handle) #keep list of handles        
    errorCode,detectionState,detectedPoint,detectedObjectHandle,detectedSurfaceNormalVector=vrep.simxReadProximitySensor(clientID,sensor_handle,vrep.simx_opmode_streaming)                
    sensor_val=np.append(sensor_val,np.linalg.norm(detectedPoint)) #get list of values

t = time.time()


flag_finish = False
while not flag_finish:
     #Loop Execution
    sensor_val=np.array([])    
    detections = np.array([]) 
    for x in range(1,8+1):
        errorCode,detectionState,detectedPoint,detectedObjectHandle,detectedSurfaceNormalVector=vrep.simxReadProximitySensor(clientID,sensor_h[x-1],vrep.simx_opmode_buffer)                
        sensor_val=np.append(sensor_val,np.linalg.norm(detectedPoint)) #get list of values
        detections=np.append(detections,detectionState) #get list of values

    errorCode,rob_pos = vrep.simxGetObjectPosition(clientID,robot_handle,origin_handle,vrep.simx_opmode_buffer)
    errorCode,end_pos = vrep.simxGetObjectPosition(clientID,end_dummy_handle,origin_handle,vrep.simx_opmode_buffer)
    errorCode,rob_ori = vrep.simxGetObjectOrientation (clientID,robot_handle,origin_handle,vrep.simx_opmode_buffer)    #define robo como origem
    rob_pos[2] = rob_ori[2]
    #rob_pos =  coordenadas x, y, theta em relacao a origem
    



    
    vect_obj_global = np.array([end_pos[0],end_pos[1], 1])      # recebe vetor objetivo em coordenadas globais
    
    A = np.array([[np.cos(rob_pos[2]), np.sin(rob_pos[2]), rob_pos[0]],
                  [-np.sin(rob_pos[2]),  np.cos(rob_pos[2]), rob_pos[1]],
                  [0,                0,                     1         ]])
    B = np.matrix.transpose(A)

    

   
    #cria cordenadas para desenhar eixos do robo
    rob_pos_global_x = np.array([1,0,1])
    rob_pos_global_y = np.array([0,1,1])
    rob_pos_global_x = np.dot(B, rob_pos_global_x) 
    rob_pos_global_y = np.dot(B, rob_pos_global_y)
    
    
    vect_obj_local = vect_obj_global - rob_pos      #recebe vetor objetivo em coordenadas locais
    
    vect_obs = detecta_pontos(imprime=False)        #chama função que retorna vetor obstáculo calculado a partir do perceptron

                             
#    vect_result = vect_obj_local + vect_obs


#A normalização de todos os vetores não é possível pois a informação de tamanho do vetor é importante pois é proporcional a quao perto encontrase do obstaculo
    #normalizando vetores 
    if vect_obj_local.any():
        vect_obj_local =    vect_obj_local  / np.linalg.norm(vect_obj_local)
        vect_obj_local = vect_obj_local*6*2
#    if vect_obs.any():
#        vect_obs =         vect_obs        / np.linalg.norm(vect_obs)
##        vect_obs = vect_obs * 2
        
    
#m normalizo apenas o vetor de resultado
    vect_result = vect_obj_local + vect_obs[0] + vect_obs[1] + vect_obs[2] + vect_obs[3] + vect_obs[4]  + vect_obs[5]  + vect_obs[6]  + vect_obs[7] 
    vect_result =       vect_result     / np.linalg.norm(vect_result)



    print np.linalg.norm(vect_obj_local), np.linalg.norm(vect_obs[0]), np.linalg.norm(vect_obs[1])



    imprime_vetores()       #imprime todos os vetores no prompt do python



    #-----------------------------
    #---- controle de rodas
    phi=math.atan2(vect_result[1],vect_result[0]) - rob_pos[2]    #phi eh anglo entre eixo x do robo e vetor resultado
    dphi =  phi
            
    v_des = 0.1         #velocidade desejada
    om_des = 0.2*dphi
    
    d = 0.380   #distance between axes
    
    v_r = (v_des+d*om_des)
    v_l = (v_des-d*om_des)
    
    wheel_radius= 0.195/2
    omega_right = v_r/wheel_radius
    omega_left = v_l/wheel_radius
    
    
    errorCode=vrep.simxSetJointTargetVelocity(clientID,left_motor_handle,omega_left, vrep.simx_opmode_streaming)
    errorCode=vrep.simxSetJointTargetVelocity(clientID,right_motor_handle,omega_right, vrep.simx_opmode_streaming)
    
    x = [i for i in np.abs(np.subtract(rob_pos, end_pos)) if i>0.4]     #pega todas as distâncias menors do que 0.2

    if not x:           #testa se string está vazia
        print "CHEGOU!"
        flag_finish = True
    
    time.sleep(0.1)

#Post ALlocation
errorCode=vrep.simxSetJointTargetVelocity(clientID,left_motor_handle,0, vrep.simx_opmode_streaming)
errorCode=vrep.simxSetJointTargetVelocity(clientID,right_motor_handle,0, vrep.simx_opmode_streaming)
    